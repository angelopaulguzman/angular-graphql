import { ArticlesModel } from './articles.model';
export interface Response{
  articles: ArticlesModel[];
}
