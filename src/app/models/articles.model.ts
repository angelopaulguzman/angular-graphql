export interface ArticlesModel {
    id?: string;
    brand?: string;
    flag?: string;
    model?: string;
    logo?: string;
    image?: string;
}
