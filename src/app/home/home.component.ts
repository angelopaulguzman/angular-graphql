import { Response } from './../models/responseArticles.model';
import { GET_ARTICLES } from './../services/articles.graphql';
import { ArticlesModel } from './../models/articles.model';
import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Apollo} from 'apollo-angular';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  articles:Observable<ArticlesModel[]> | undefined;
  constructor(private apollo:Apollo) { }

  ngOnInit(): void {
    this.synch();
  }
  synch():void{
    this.articles=this.apollo.watchQuery<Response>( {
      query:GET_ARTICLES,
    }).valueChanges.pipe(
      map((result) =>result.data.articles)
    );
  }
}
