import gql from 'graphql-tag';

export const GET_ARTICLES= gql`
{
  articles{
    id
    brand
    flag
    model
    logo
    image
  }
}
`;
